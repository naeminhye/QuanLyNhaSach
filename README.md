# QuanLyNhaSach - website
đây là project của nhóm các sinh viên trường Đại học Công nghệ Thông tin - Đại học Quốc gia thành phố Hồ Chí Minh

## Công nghệ sử dụng:
- ASP.NET MVC5
- Entity Framework 6.2 (Code first)

## Website:
- http://bookstoreuit.azurewebsites.net/

## Account
- admin : daylapass
- thungan : daylapass

## Cài đặt
- Microsoft Visual Studio 2017
- .NET Framework 4.6.1

- Database đã được connect sẵn (và dùng chung với website nói trên)
- Mở (project)/Views/Home/Index.cshtml và Ctrl + F5 (build & run)
